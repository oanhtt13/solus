
//
//  UserDataManager.swift
//  Koleda
//
//  Created by Oanh tran on 7/15/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation

enum TemperatureUnit: String{
    case F = "F"
    case C = "C"
    case unknow
    
    init(fromString string: String) {
        guard let value = TemperatureUnit(rawValue: string) else {
            self = .unknow
            return
        }
        self = value
    }
}

final class UserDataManager {
    static let shared = UserDataManager()
    var currentUser: User?
    var rooms : [Room]
    var tariff: Tariff?
    var wifiInfo: WifiInfo?
    var deviceModelList: [String]
    var temperatureUnit: TemperatureUnit
    var energyConsumed: Double
    var settingModes: [ModeItem]
    var smartScheduleData: [String:ScheduleOfDay]
    
    
    private init() {
        rooms = []
        deviceModelList = []
        temperatureUnit = .C
        energyConsumed = 0
        smartScheduleData = [:]
        settingModes = []
    }
    
    func clearUserData() {
        rooms = []
        currentUser = nil
        tariff =  nil
        wifiInfo = nil
        deviceModelList = []
        temperatureUnit = .C
        energyConsumed = 0
        smartScheduleData = [:]
        settingModes = []
    }
    
    func roomWith(roomId: String) -> Room? {
        return rooms.first(where: { $0.id == roomId })
    }
    
    func settingModesWithoutDefaultMode() -> [ModeItem] {
        return settingModes.filter { $0.mode != .DEFAULT}
    }
}
