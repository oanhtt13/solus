//
//  UserManager.swift
//  Koleda
//
//  Created by Oanh tran on 7/11/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import PromiseKit
import GoogleSignIn

protocol UserManager {
    func getCurrentUser(success: (() -> Void)?, failure: ((Error) -> Void)?)
    func logOut(completion: @escaping () -> Void)
}


class UserManagerImpl: UserManager {
    private let sessionManager: SessionManager
    
    private func baseURL() -> URL {
        return UrlConfigurator.urlByAdding()
    }
    
    init(sessionManager: SessionManager) {
        self.sessionManager =  sessionManager
    }
    
    func getCurrentUser(success: (() -> Void)?, failure: ((Error) -> Void)? = nil) {
        guard let request = try? URLRequest(url: baseURL().appendingPathComponent("user/me"), method: .get) else {
            assertionFailure()
            DispatchQueue.main.async {
                failure?(WSError.general)
            }
            return
        }
        
        sessionManager
            .request(request).validate().responseData { response in
                switch response.result {
                case .success(let data):
                    do {
                        let decodedJSON = try JSONDecoder().decode(User.self, from: data)
                        UserDataManager.shared.currentUser = decodedJSON
                        UserDataManager.shared.temperatureUnit = TemperatureUnit(fromString: UserDataManager.shared.currentUser?.temperatureUnit ?? "C")
                        success?()
                    } catch {
                        log.info("Get Current User parsing error: \(error)")
                        failure?(error)
                    }
                case .failure(let error):
                    log.info("Get Current User fetch error: \(error)")
                    failure?(error)
                }
        }
    }
    
    private func logOutRemotely() -> Promise<Void> {
        return Promise { fulfill, reject in
            let endPointURL = baseURL().appendingPathComponent("auth/logout")
            guard let request = URLRequest.postRequestWithJsonBody(url: endPointURL, parameters: [:]) else {
                reject(RequestError.error(NSLocalizedString("Failed to send request, please try again later", comment: "")))
                return
            }
            
            sessionManager.request(request).validate().response { response in
                if let error = response.error {
                    if let error = error as? URLError, error.code == URLError.notConnectedToInternet {
                        NotificationCenter.default.post(name: .KLDNotConnectedToInternet, object: error)
                    } else {
                        log.error("Failed performing logout")
                    }
                    reject(error)
                } else {
                    fulfill(())
                }
            }
        }
    }
    
    func logOut(completion: @escaping () -> Void) {
        firstly {
                return cancelAllTasks()
            }.then {
                return self.logOutRemotely()
            }.always {
                UserDefaultsManager.removeUserValues()
                try? LocalAccessToken.delete()
                try? RefreshToken.delete()
                UserDataManager.shared.clearUserData()
                log.info("Logged out")
                completion()
        }
    }
    
    private func signOutGoogle() {
        do {
            try GIDSignIn.sharedInstance().signOut()
                GIDSignIn.sharedInstance().disconnect()
            
            if let url = NSURL(string:  "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=https://google.com"){
                UIApplication.shared.open(url as URL, options: [:]) { (true) in
                }
            }
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    private func cancelAllTasks() -> Promise<Void> {
        return Promise { fulfill, reject in
            sessionManager.session.getAllTasks { tasks in
                tasks.forEach { $0.cancel() }
                
                log.info("Cancelled all tasks")
                fulfill(())
            }
        }
    }
    
}

