//
//  AddHeaterViewController.swift
//  Koleda
//
//  Created by Oanh tran on 9/4/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD

class AddHeaterViewController:  BaseViewController, BaseControllerProtocol {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detectOneHeaterView: UIView!
    @IBOutlet weak var noHeaterLabel: UILabel!
    @IBOutlet weak var noHeaterView: UIView!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var tryAgain2Button: UIButton!
    @IBOutlet weak var connectToDeviceHotspotButton: UIButton!
    
    @IBOutlet weak var searchingHeaterView: UIView!
    @IBOutlet weak var moreThanOneHeaterView: UIView!
    @IBOutlet weak var couldNotFindHeaterView: UIView!
    @IBOutlet weak var reportSearchHeaterView: UIView!
    @IBOutlet weak var haveFoundTitleView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var aHeaterView: UIView!
    @IBOutlet weak var deviceModelLabel: UILabel!
    @IBOutlet weak var addHeaterButton: UIButton!
    @IBOutlet weak var addingHeaterTitleView: UIView!
    @IBOutlet weak var addSuccessfullyView: UIView!
    @IBOutlet weak var addedDeviceModelLabel: UILabel!
    @IBOutlet weak var addedRoomNameLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    var viewModel: AddHeaterViewModelProtocol!
    var isFromRoomConfiguration: Bool = false
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarTransparency()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        startToDectectDevice()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func scanShellyDevices() {
        startToDectectDevice()
    }
    
    private func configurationUI() {
        cancelButton.isHidden = true
        canBackToPreviousScreen = false
        NotificationCenter.default.addObserver(self, selector: #selector(scanShellyDevices),
                                               name: .KLDDidChangeWifi, object: nil)
        detectOneHeaterView.isHidden = true
        noHeaterView.isHidden = true
        SVProgressHUD.setDefaultStyle(.dark)
        cancelButton.rx.tap.bind { [weak self] in
            self?.back()
        }.disposed(by: disposeBag)
        viewModel.cancelButtonHidden.asObservable().bind(to: cancelButton.rx.isHidden).disposed(by: disposeBag)
        viewModel.stepAddHeaters.asObservable().subscribe(onNext: { [weak self] status in
            self?.searchingHeaterView.isHidden = true
            self?.moreThanOneHeaterView.isHidden = true
            self?.couldNotFindHeaterView.isHidden = true
            self?.reportSearchHeaterView.isHidden = true
            self?.haveFoundTitleView.isHidden = true
            self?.lineView.isHidden = true
            self?.aHeaterView.isHidden = true
            self?.addingHeaterTitleView.isHidden = true
            self?.addSuccessfullyView.isHidden = true
            
            switch status {
            case .search:
                self?.searchingHeaterView.isHidden = false
                self?.viewModel.cancelButtonHidden.onNext(true)
            case .moreThanOneDevice: //more than one heater 1
                self?.moreThanOneHeaterView.isHidden = false
                self?.viewModel.cancelButtonHidden.onNext(true)
            case .noDevice: //no heater 2
                self?.couldNotFindHeaterView.isHidden = false
                self?.viewModel.cancelButtonHidden.onNext(true)
            case .oneDevice: //found a heater 3
                self?.reportSearchHeaterView.isHidden = false
                self?.haveFoundTitleView.isHidden = false
                self?.lineView.isHidden = false
                self?.aHeaterView.isHidden = false
                self?.deviceModelLabel.text = self?.viewModel.detectedHeaters[0].deviceModel
                self?.addHeaterButton.isEnabled = true
                self?.viewModel.cancelButtonHidden.onNext(true)
            case .addDevice: //adding a heater 4
                self?.reportSearchHeaterView.isHidden = false
                self?.addingHeaterTitleView.isHidden = false
                self?.lineView.isHidden = false
                self?.aHeaterView.isHidden = false
                self?.deviceModelLabel.text = self?.viewModel.detectedHeaters[0].deviceModel
                self?.addHeaterButton.isEnabled = false
                self?.viewModel.cancelButtonHidden.onNext(false)
            case .addDeviceSuccess: //adding a heater successfully 5
                self?.reportSearchHeaterView.isHidden = false
                self?.addSuccessfullyView.isHidden = false
                self?.deviceModelLabel.text = self?.viewModel.detectedHeaters[0].deviceModel
                self?.addHeaterButton.isEnabled = false
                self?.addedDeviceModelLabel.text = self?.viewModel.detectedHeaters[0].deviceModel
                self?.viewModel.cancelButtonHidden.onNext(true)
                if let userName = UserDataManager.shared.currentUser?.name, let roomName = self?.viewModel.roomName {
                    self?.addedRoomNameLabel.text = "\(userName)’s \(roomName)"
                } else {
                    self?.addedRoomNameLabel.text = self?.viewModel.roomName
                }
            case .joinDeviceHotSpot:
                self?.viewModel.cancelButtonHidden.onNext(true)
                guard let ssid = UserDefaultsManager.wifiSsid.value, let pass = UserDefaultsManager.wifiPass.value else {
                    self?.viewModel.stepAddHeaters.onNext(.noDevice)
                    self?.app_showInfoAlert("Please update the Wifi Setting info.", title: "Koleda", completion: {
                        self?.viewModel.showWifiDetail()
                    })
                    return
                }
                SVProgressHUD.show()
                self?.viewModel.connectHeaterLocalWifi(ssid: ssid, pass: pass, completion: { [weak self] success in
                    SVProgressHUD.dismiss()
                    self?.viewModel.stepAddHeaters.onNext(.noDevice)
                    if success {
                        self?.app_showInfoAlert("Heater connected to your local wifi.", title: "Successfull!", completion: {
                            SVProgressHUD.show()
                            self?.viewModel.waitingHeatersJoinNetwork(completion: {
                                SVProgressHUD.dismiss()
                                self?.startToDectectDevice()
                            })
                        })
                    } else {
                        self?.viewModel.stepAddHeaters.onNext(.noDevice)
                        self?.app_showInfoAlert("Wifi ssid or password is incorect, Please check again.", title: "Error", completion: {
                            self?.viewModel.showWifiDetail()
                        })
                    }
                })
            }
            
        }).disposed(by: disposeBag)
        
        addHeaterButton.rx.tap.bind { [weak self] in
            self?.viewModel.stepAddHeaters.onNext(.addDevice)
            self?.viewModel.addAHeaterToARoom { (error, deviceModel, roomName) in
                guard let error = error else {
                    NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                    self?.viewModel.stepAddHeaters.onNext(.addDeviceSuccess)
                    return
                }
                if error == WSError.deviceExisted {
                    self?.app_showAlertMessage(title: "Error", message: String(format: "This heater %@", error.localizedDescription))
                } else {
                    self?.app_showAlertMessage(title: "Error", message: "Can not add Heater.")
                }
            }
        }.disposed(by: disposeBag)
        
        tryAgain2Button.rx.tap.bind { [weak self] in
            self?.noHeaterView.isHidden = true
            self?.startToDectectDevice()
        }.disposed(by: disposeBag)
        tryAgainButton.rx.tap.bind { [weak self] in
            self?.noHeaterView.isHidden = true
            self?.startToDectectDevice()
        }.disposed(by: disposeBag)
        
        connectToDeviceHotspotButton.rx.tap.bind { [weak self] in
            self?.goWifiSetting()
        }.disposed(by: disposeBag)
        
        viewModel.reSearchingHeater.asObservable().subscribe(onNext: { [weak self] success in
            self?.viewModel.stepAddHeaters.onNext(.noDevice)
            self?.startToDectectDevice()
        }).disposed(by: disposeBag)
        
        NotificationCenter.default.addObserver(self, selector: #selector(scanShellyDevices),
                                               name: .KLDNeedToReSearchDevices, object: nil)
    }
    
    private func goWifiSetting() {
        let OkAction = UIAlertAction(title: "OK".app_localized, style: .default) { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:])
            }
        }
        let cancelAction = UIAlertAction(title: "CANCEL".app_localized, style: .cancel) { action in
            
        }
        app_showAlertMessage(title: "Koleda".app_localized,
                             message: "You will be directed to your Solus+ app's general settings.  Please navigate manually to Wifi-settings and select your heater's hotspot.  Once selected, you can return to your Solus+ app.".app_localized, actions: [cancelAction, OkAction])
    }
    
    private func startToDectectDevice() {
        self.view.endEditing(true)
        self.viewModel.stepAddHeaters.onNext(.search)
        noHeaterView.isHidden = true
        let ssid = viewModel.getCurrentWiFiName()
        print(ssid)
        guard FGRoute.getGatewayIP() != nil else {
            if ssid != "" && DataValidator.isShellyHeaterDevice(hostName: ssid) {
                SVProgressHUD.show()
                viewModel.fetchInfoOfHeaderAPMode(completion: { [weak self] success in
                    SVProgressHUD.dismiss()
                    if success {
                        self?.viewModel.stepAddHeaters.onNext(.joinDeviceHotSpot)
                    } else {
                        self?.viewModel.stepAddHeaters.onNext(.noDevice)
                    }
                })
            } else {
                viewModel.stepAddHeaters.onNext(.noDevice)
                NotificationCenter.default.post(name: .KLDNotConnectedToInternet, object: nil)
            }
            return
        }
        closeButton?.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel.findHeatersOnLocalNetwork {
                if self.viewModel.detectedHeaters.count == 0 {
                    self.viewModel.stepAddHeaters.onNext(.noDevice)
                } else if self.viewModel.detectedHeaters.count > 1 {
                    self.viewModel.stepAddHeaters.onNext(.moreThanOneDevice)
                } else if self.viewModel.detectedHeaters.count == 1 {
                    self.viewModel.stepAddHeaters.onNext(.oneDevice)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isFromRoomConfiguration {
            backToViewControler(viewController: HeatersManagementViewController.self)
        } else {
            backToRoot(animated: true)
        }
    }
    @IBAction func yesAction(_ sender: Any) {
        startToDectectDevice()
    }
    
    @IBAction func noAction(_ sender: Any) {
        if addSuccessfullyView.isHidden == false {
            if isFromRoomConfiguration {
                backToViewControler(viewController: HeatersManagementViewController.self)
            } else {
                backToRoot(animated: true)
            }
        } else {
            back()
        }
    }
}

extension AddHeaterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
