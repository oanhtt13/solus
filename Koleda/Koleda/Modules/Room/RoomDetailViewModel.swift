//
//  RoomDetailViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 7/9/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import SVProgressHUD

protocol RoomDetailViewModelProtocol: BaseViewModelProtocol {
    var screenTitle: Variable<String> { get }
    var roomTypes: Variable<[RoomType]>  { get }
    var roomName: Variable<String> { get }
    var roomNameErrorMessage: Variable<String> { get }
    var showPopUpSelectRoomType: PublishSubject<Bool> { get }
    var showPopUpSuccess: PublishSubject<Bool> { get }
    var hideDeleteButton: BehaviorSubject<Bool> { get }
    var selectedCategory: Variable<String> { get }
    var nextButtonTitle: Variable<String> { get }
    var currentRoomType: RoomType? { get }
    
    func viewWillAppear()
    func roomType(at indexPath: IndexPath?) -> RoomType?
    func roomType(with category: String) -> RoomType?
    func indexPathOfCategory(with category: String) -> IndexPath?
    func next(roomType: RoomType?)
    func deleteRoom()
}

class RoomDetailViewModel: BaseViewModel, RoomDetailViewModelProtocol {
    let router: BaseRouterProtocol
    var currentRoomType :RoomType?
    let screenTitle = Variable<String>("Add a Room")
    let roomTypes = Variable<[RoomType]>([])
    let roomName = Variable<String>("")
    let roomNameErrorMessage = Variable<String>("")
    let showPopUpSelectRoomType = PublishSubject<Bool>()
    let showPopUpSuccess = PublishSubject<Bool>()
    let hideDeleteButton = BehaviorSubject<Bool>(value: true)
    let selectedCategory = Variable<String>("")
    let nextButtonTitle = Variable<String>("Create Room")
    
    private var editingRoom: Room?
    private var roomManager: RoomManager
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance, editingRoom: Room? = nil) {
        self.router = router
        self.roomManager = managerProvider.roomManager
        self.editingRoom = editingRoom
        super.init(managerProvider: managerProvider)
        self.roomTypes.value = RoomType.initRoomTypes()
        if let category = editingRoom?.category {
            self.currentRoomType = roomType(with: category)
        }
    }
    
    func viewWillAppear() {
        guard let name = editingRoom?.name, let category = editingRoom?.category else {
            return
        }
        screenTitle.value = "Edit Room"
        nextButtonTitle.value = "Confirm Room Details"
        roomName.value = name
        selectedCategory.value = category
        hideDeleteButton.onNext(false)
    }
    
    func roomType(at indexPath: IndexPath?) -> RoomType? {
        guard let selectedIndexpath = indexPath else {
            return nil
        }
        return roomTypes.value[selectedIndexpath.item]
    }
    
    func roomType(with category: String) -> RoomType? {
        let roomCategory = RoomCategory(fromString: category)
        return roomTypes.value.filter { $0.category == roomCategory}.first
    }
    
    func indexPathOfCategory(with category: String) -> IndexPath? {
        guard let roomType = self.roomType(with: category), let item = roomTypes.value.firstIndex(where: { $0 == roomType }) else { return nil }
        return IndexPath(item: item, section: 0)
    }
    
    func next(roomType: RoomType?) {
        guard let type = roomType else {
            self.showPopUpSelectRoomType.onNext(true)
            return
        }
        let roomName = self.roomName.value.extraWhitespacesRemoved
        let categoryString = type.category.rawValue
        if validateRoomName() {
            guard let editingRoom = editingRoom else {
                addRoom(category: categoryString, name: roomName)
                return
            }
            if editingRoom.name != roomName || editingRoom.category != type.category.rawValue {
                updateRoom(roomId: editingRoom.id, category: categoryString, name: roomName)
            }
        }
    }
    
    func deleteRoom() {
        guard let roomId = self.editingRoom?.id else {
            return
        }
        roomManager.deleteRoom(roomId: roomId, success: { [weak self] in
            NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
            self?.router.dismiss(animated:true, context: RoomDetailRouter.RouteType.deleted, completion: nil)
        },
        failure: { error in })
    }
}

extension RoomDetailViewModel {
    
    private func validateRoomName() -> Bool {
        if roomName.value.extraWhitespacesRemoved.isEmpty {
            roomNameErrorMessage.value = "Room Name is not Empty"
            return false
        } else {
            roomNameErrorMessage.value = ""
            return true
        }
    }
    
    private func addRoom(category: String, name: String) {
        SVProgressHUD.show()
        roomManager.createRoom(category: category, name: name, success: { [weak self] roomId in
            SVProgressHUD.dismiss(completion: {
                NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                self?.router.enqueueRoute(with: RoomDetailRouter.RouteType.added(roomId, name))
            })
        },
        failure: { _ in
            SVProgressHUD.dismiss()
        })
    }
    
    private func updateRoom(roomId: String, category: String, name: String) {
        SVProgressHUD.show(withStatus: "updating room")
        roomManager.updateRoom(roomId: roomId, category: category, name: name, success: { [weak self] in
            SVProgressHUD.dismiss(completion: {
                NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                self?.router.dismiss(animated: true, context: RoomDetailRouter.RouteType.updated, completion: nil)
            })
        },
        failure: { error in
            SVProgressHUD.dismiss()
        })
    }
    
    
}
