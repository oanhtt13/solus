//
//  AlertConfirmViewController.swift
//  Koleda
//
//  Created by Vu Xuan Hoa on 9/14/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit

enum TypeAlert {
    case deleteRoom(icon: UIImage, roomName: String)
    case confirmDeleteRoom()
    case deleteSensor(sensorName: String)
    case confirmDeleteSensor()
    case deleteHeater(heaterName: String)
    case confirmDeleteHeater()
}

typealias OnClickLetfButton = () -> Void
typealias OnClickRightButton = () -> Void

class AlertConfirmViewController: UIViewController {

    @IBOutlet weak var deleteRoomView: UIView!
    @IBOutlet weak var roomImageView: UIImageView!
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var confirmDeleteRoomView: UIView!
    
    @IBOutlet weak var deleteSensorView: UIView!
    @IBOutlet weak var sensorImageView: UIImageView!
    @IBOutlet weak var sensorNameLabel: UILabel!
    
    @IBOutlet weak var confirmDeleteSensorView: UIView!
    @IBOutlet weak var deleteHeaterView: UIView!
    @IBOutlet weak var heaterNameLabel: UILabel!
    @IBOutlet weak var confirmDeleteHeaterView: UIView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var typeAlert: TypeAlert!
    
    var onClickLetfButton: OnClickLetfButton?
    var onClickRightButton: OnClickRightButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI(type: typeAlert)
    }
    
    func initView() {
        deleteRoomView.isHidden = true
        confirmDeleteRoomView.isHidden = true
        deleteSensorView.isHidden = true
        confirmDeleteSensorView.isHidden = true
        deleteHeaterView.isHidden = true
        confirmDeleteHeaterView.isHidden = true
    }
    
    private func updateUI(type: TypeAlert) {
        initView()
        switch type {
        case .deleteRoom(let icon, let roomName):
            deleteRoomView.isHidden = false
            roomImageView.image = icon
            roomNameLabel.text = roomName
        case .confirmDeleteRoom():
            confirmDeleteRoomView.isHidden = false
            leftButton.setTitle("Cancel", for: .normal)
            rightButton.setTitle("Delete", for: .normal)
            rightButton.setTitleColor(UIColor(hex: 0xFF7020), for: .normal)
        case .deleteSensor(let sensorName):
            deleteSensorView.isHidden = false
            sensorNameLabel.text = sensorName
        case .confirmDeleteSensor():
            confirmDeleteSensorView.isHidden = false
            leftButton.setTitle("Cancel", for: .normal)
            rightButton.setTitle("Delete", for: .normal)
            rightButton.setTitleColor(UIColor(hex: 0xFF7020), for: .normal)
        case .deleteHeater(let  heaterName):
            deleteHeaterView.isHidden = false
            heaterNameLabel.text = heaterName
        case .confirmDeleteHeater():
            confirmDeleteHeaterView.isHidden = false
            leftButton.setTitle("Cancel", for: .normal)
            rightButton.setTitle("Delete", for: .normal)
            rightButton.setTitleColor(UIColor(hex: 0xFF7020), for: .normal)
        }
    }

    @IBAction func leftClickAction(_ sender: Any) {
        back(animated: false)
        onClickLetfButton?()
    }
    @IBAction func rightClickAction(_ sender: Any) {
        back(animated: false)
        onClickRightButton?()
    }
}
