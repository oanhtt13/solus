//
//  LoginViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 6/11/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FacebookLogin
import FacebookCore
import SVProgressHUD
import GoogleSignIn



protocol LoginViewModelProtocol: BaseViewModelProtocol {
    
    var email: Variable<String> { get }
    var password: Variable<String> { get }
    var emailErrorMessage: Variable<String> { get }
    var passwordErrorMessage: Variable<String> { get }
    
    var showPassword: Variable<Bool> { get }
    
    func login(completion: @escaping (Bool) -> Void)
    func showPassword(isShow: Bool)
    func prepare(for segue: UIStoryboardSegue)
    func forgotPassword()
    func loginWithSocial(type: SocialType, accessToken: String, completion: @escaping (Bool, WSError?) -> Void)
}

class LoginViewModel: BaseViewModel, LoginViewModelProtocol {
    let email = Variable<String>("")
    let password = Variable<String>("")
    let emailErrorMessage = Variable<String>("")
    let passwordErrorMessage = Variable<String>("")
    
    let showPassword = Variable<Bool>(false)
    
    
    let router: BaseRouterProtocol
    
    private var _isShowPass = BehaviorSubject(value: false)
    
    private let loginAppManager: LoginAppManager
    private var disposeBag = DisposeBag()
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        loginAppManager = managerProvider.loginAppManager
        super.init(managerProvider: managerProvider)
    }
    
    func forgotPassword() {
        router.enqueueRoute(with: LoginRouter.RouteType.forgotPassword)
    }
    
    func showPassword(isShow: Bool) {
        showPassword.value = isShow
    }
    
    func login(completion: @escaping (Bool) -> Void) {
        guard validateAll() else {
            completion(true)
            return
        }
        
        loginAppManager.login(email: email.value.extraWhitespacesRemoved,
                              password: password.value.extraWhitespacesRemoved,
                              success: { [weak self] in
                                log.info("User signed in successfully")
                                self?.processAfterLoginSuccessfull()
                                completion(true)
            },
                              failure: { error in
                                completion(false)
        })
    }
    
    func prepare(for segue: UIStoryboardSegue) {
        router.prepare(for: segue)
    }
    
    func loginWithSocial(type: SocialType, accessToken: String, completion: @escaping (Bool, WSError?) -> Void) {
        loginAppManager.loginWithSocial(type: type, accessToken: accessToken,
        success: { [weak self] in
            log.info("User signed in successfully")
            self?.router.enqueueRoute(with: OnboardingRouter.RouteType.termAndConditions)
            completion(true, nil)
        }, failure: { error in
            completion(false, error as? WSError)
        })
    }
}



extension LoginViewModel {
    
    private func processAfterLoginSuccessfull() {
        guard let termAndConditionAcceptedUser = UserDefaultsManager.termAndConditionAcceptedUser.value?.extraWhitespacesRemoved, termAndConditionAcceptedUser == email.value.extraWhitespacesRemoved else {
            router.enqueueRoute(with: LoginRouter.RouteType.termAndConditions)
            return
        }
        router.enqueueRoute(with: LoginRouter.RouteType.home)
        UserDefaultsManager.loggedIn.enabled = true
    }
    
    private func validateAll() -> Bool {
        var failCount = 0
        failCount += validateEmail() ? 0 : 1
        failCount += validatePassword() ? 0 : 1
        return failCount == 0
    }
    
    private func validateEmail() -> Bool {
        if email.value.extraWhitespacesRemoved.isEmpty {
            emailErrorMessage.value = "Email is not Empty"
            return false
        }
        if DataValidator.isEmailValid(email: email.value.extraWhitespacesRemoved) {
            emailErrorMessage.value = ""
            return true
        } else {
            emailErrorMessage.value = "Email is invalid"
            return false
        }
    }
    
    private func validatePassword() -> Bool {
        if password.value.extraWhitespacesRemoved.isEmpty {
            passwordErrorMessage.value = "Password is not Empty"
            return false
        }
        
        if DataValidator.isEmailPassword(pass: password.value.extraWhitespacesRemoved)
        {
            passwordErrorMessage.value = ""
            return true
        } else {
            passwordErrorMessage.value = "INVALID_PASSWORD_MESSAGE".app_localized
            return false
        }
    }
}
