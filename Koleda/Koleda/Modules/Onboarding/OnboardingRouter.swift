//
//  OnboardingRouter.swift
//  Koleda
//
//  Created by Oanh tran on 5/23/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit

class OnboardingRouter: BaseRouterProtocol {
    
    enum RouteType {
        case signUp
        case logIn
        case termAndConditions
    }
    
    weak var baseViewController: UIViewController?
    
    
    func enqueueRoute(with context: Any?, animated: Bool, completion: ((Bool) -> Void)?) {
        guard let routeType = context as? RouteType else {
            assertionFailure("The route type missmatches")
            return
        }
        
        guard let baseViewController = baseViewController else {
            assertionFailure("baseViewController is not set")
            return
        }
        
        switch routeType {
        case .signUp:
            baseViewController.performSegue(withIdentifier: SignUpViewController.get_identifier, sender: self)
            break
        case .logIn:
            let router = LoginRouter()
            guard let viewController = StoryboardScene.Login.initialViewController() as? LoginViewController else { return }
            let viewModel = LoginViewModel.init(router: router)
            viewController.viewModel = viewModel
            router.baseViewController = viewController
            baseViewController.navigationController?.pushViewController(viewController, animated: true)
            break
        case .termAndConditions:
            let router = TermAndConditionRouter()
            let viewModel = TermAndConditionViewModel.init(router: router)
            guard let viewController = StoryboardScene.Setup.instantiateTermAndConditionViewController() as? TermAndConditionViewController else { return }
            viewController.viewModel = viewModel
            router.baseViewController = viewController
            baseViewController.navigationController?.pushViewController(viewController, animated: true)
//        case .syncUser:
//            let router = SyncRouter()
//            guard let viewController = StoryboardScene.Setup.initialViewController() as? SyncViewController else { return }
//            let viewModel = SyncViewModel.init(with: router)
//            viewController.viewModel = viewModel
//            router.baseViewController = viewController
//            baseViewController.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func present(on baseVC: UIViewController, animated: Bool, context: Any?, completion: ((Bool) -> Void)?) {
        
    }
    
    func dismiss(animated: Bool, context: Any?, completion: ((Bool) -> Void)?) {
        
    }
    
    func prepare(for segue: UIStoryboardSegue) {
        if let viewController = segue.destination as? SignUpViewController {
            let router = SignUpRouter()
            let viewModel = SignUpViewModel.init(router: router)
            viewController.viewModel = viewModel
            router.baseViewController = viewController
        }
    }
}
