//
//  SelectedRoomViewController.swift
//  Koleda
//
//  Created by Oanh tran on 8/26/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD
import SwiftRichString

class SelectedRoomViewController: BaseViewController, BaseControllerProtocol {
    @IBOutlet weak var userHomeTitle: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var adjustTempLabel: UILabel!
    @IBOutlet weak var configurationButton: UIButton!

    @IBOutlet weak var ecoModeView: ModeView!
    @IBOutlet weak var ecoModeButton: UIButton!
    @IBOutlet weak var nightModeView: ModeView!
    @IBOutlet weak var nightModeButton: UIButton!
    @IBOutlet weak var comfortModeView: ModeView!
    @IBOutlet weak var comfortModeButton: UIButton!
    @IBOutlet weak var smartScheduleModeView: ModeView!
    @IBOutlet weak var smartScheduleModeButton: UIButton!
    
    @IBOutlet weak var onOffSwitchButton: UIButton!
    @IBOutlet weak var onOffSwitchLabel: UILabel!
    @IBOutlet weak var onOffSwitchImageView: UIImageView!
   
    
    
    @IBOutlet weak var topIncreaseConstraint: NSLayoutConstraint!
    @IBOutlet weak var topHeatingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topDecreaseConstraint: NSLayoutConstraint!
    @IBOutlet weak var heighIncreaseConstraint: NSLayoutConstraint!
    private let disposeBag = DisposeBag()
    
    var viewModel: SelectedRoomViewModelProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarTransparency()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        viewModel.setup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func needUpdateSelectedRoom() {
        viewModel.needUpdateSelectedRoom()
    }
    
    @IBAction func turnOnOffAction(_ sender: Any) {
        SVProgressHUD.show()
        viewModel.turnOnOrOffRoom { [weak self] turnOn, isSuccess in
            SVProgressHUD.dismiss()
            if !isSuccess {
                let status = turnOn ? "turn on" : "turn off"
                self?.app_showInfoAlert("You can't \(status) this room now. Please try again later.")
            }
        }
    }
    
    private func configurationUI() {
        var topIncrease: CGFloat = 73
        var topHeating: CGFloat = 26
        var topDecrease: CGFloat = 26
        var heighIncrease: CGFloat = 45
        if UIDevice.screenType == .iPhones_5_5s_5c_SE {
            topIncrease = 42
            topHeating = 16
            topDecrease = 8
            heighIncrease = 35
            let adjustTempFont = UIFont.app_FuturaPTDemi(ofSize: 45)
            adjustTempLabel.font = adjustTempFont
        }
        
        topIncreaseConstraint.constant = topIncrease
        topHeatingConstraint.constant = topHeating
        topDecreaseConstraint.constant = topDecrease
        heighIncreaseConstraint.constant = heighIncrease
        
        NotificationCenter.default.addObserver(self, selector: #selector(needUpdateSelectedRoom),
                                               name: .KLDNeedUpdateSelectedRoom, object: nil)
        increaseButton.rx.tap.bind { [weak self] _ in
            self?.viewModel.showManualBoostScreen()
        }.disposed(by: disposeBag)
        decreaseButton.rx.tap.bind { [weak self] _ in
            self?.viewModel.showManualBoostScreen()
        }.disposed(by: disposeBag)
        configurationButton.rx.tap.bind { [weak self] _ in
            self?.viewModel.showConfigurationScreen()
        }.disposed(by: disposeBag)
        viewModel.homeTitle.asObservable().bind(to: userHomeTitle.rx.text).disposed(by: disposeBag)
        viewModel.temperature.asObservable().bind(to: temperatureLabel.rx.text).disposed(by: disposeBag)
        viewModel.humidity.asObservable().bind(to: humidityLabel.rx.text).disposed(by: disposeBag)
        viewModel.editingTemprature.asObservable().bind { [weak self] value in
            self?.adjustTempLabel.text = "\(value)"
        }.disposed(by: disposeBag)
        viewModel.canAdjustTemp.asObservable().subscribe(onNext: { [weak self] canAdjust in
            self?.increaseButton.isEnabled = canAdjust
            self?.decreaseButton.isEnabled = canAdjust
        }).disposed(by: disposeBag)
        viewModel.modeItems.asObservable().bind { [weak self] modeItems in
            guard let eco =  ModeItem.getModeItem(with: .ECO), let night = ModeItem.getModeItem(with: .NIGHT), let comfort = ModeItem.getModeItem(with: .COMFORT) else {
                return
            }
            self?.ecoModeView.setUp(modeItem: eco)
            self?.nightModeView.setUp(modeItem: night)
            self?.comfortModeView.setUp(modeItem: comfort)
            let smartSchedule = ModeItem.getSmartScheduleMode()
            self?.smartScheduleModeView.setUp(modeItem: smartSchedule)
        }.disposed(by: disposeBag)
        
        ecoModeButton.rx.tap.bind { [weak self] _ in
            self?.checkBeforeUpdateSmartMode(selectedMode: .ECO)
        }.disposed(by: disposeBag)
        comfortModeButton.rx.tap.bind { [weak self] _ in
            self?.checkBeforeUpdateSmartMode(selectedMode: .COMFORT)
        }.disposed(by: disposeBag)
        nightModeButton.rx.tap.bind { [weak self] _ in
            self?.checkBeforeUpdateSmartMode(selectedMode: .NIGHT)
        }.disposed(by: disposeBag)
        smartScheduleModeButton.rx.tap.bind { [weak self] _ in
            self?.checkBeforeUpdateSmartMode(selectedMode: .SMARTSCHEDULE)
        }.disposed(by: disposeBag)
        
        viewModel.ecoModeUpdate.asObservable().subscribe(onNext: { [weak self] enable in
            self?.ecoModeView.updateStatus(enable: enable)
        }).disposed(by: disposeBag)
        viewModel.comfortModeUpdate.asObservable().subscribe(onNext: { [weak self] enable in
            self?.comfortModeView.updateStatus(enable: enable)
        }).disposed(by: disposeBag)
        viewModel.nightModeUpdate.asObservable().subscribe(onNext: { [weak self] enable in
            self?.nightModeView.updateStatus(enable: enable)
        }).disposed(by: disposeBag)
        viewModel.smartScheduleModeUpdate.asObservable().subscribe(onNext: { [weak self] enable in
            self?.smartScheduleModeView.updateStatus(enable: enable)
        }).disposed(by: disposeBag)
        viewModel.turnOnRoom.asObservable().subscribe(onNext: { [weak self] turnOn in
            self?.onOffSwitchImageView.image = UIImage(named: turnOn ? "ic-switch-on": "ic-switch-off")
            self?.onOffSwitchButton.isSelected = turnOn
            self?.onOffSwitchLabel.text = turnOn ? "On" : "OFF"
            self?.onOffSwitchLabel.textColor = turnOn ? UIColor.black : UIColor.gray
        }).disposed(by: disposeBag)
        
    }
    
    private func checkBeforeUpdateSmartMode(selectedMode: SmartMode) {
        guard viewModel.heaters.count > 0  else {
            app_showInfoAlert("Changing modes is not possible. Please connect a heater to your room, first")
            return
        }
        
        if viewModel.settingType == .MANUAL {
            app_showInfoAlert("You can't update the mode, because manual boost is on")
        } else if selectedMode == .SMARTSCHEDULE {
            updateScheduleMode()
        } else {
            updateSettingMode(selectedMode: selectedMode)
        }
    }
    
    private func updateSettingMode(selectedMode: SmartMode) {
        SVProgressHUD.show()
        viewModel.updateSettingMode(mode: selectedMode) { [weak self] (updatedMode, isSuccess, errorMessage)  in
            SVProgressHUD.dismiss()
            if isSuccess {
                NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                self?.viewModel.changeSmartMode(seletedSmartMode: updatedMode)
            } else {
                if !errorMessage.isEmpty {
                    self?.app_showInfoAlert(errorMessage)
                }
            }
        }
    }
    
    private func updateScheduleMode() {
        SVProgressHUD.show()
        viewModel.turnOnOrOffScheduleMode(completion: { [weak self] (isSuccess, errorMessage) in
            SVProgressHUD.dismiss()
            if isSuccess {
                NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                self?.viewModel.changeSmartMode(seletedSmartMode: .SMARTSCHEDULE)
            } else {
                if !errorMessage.isEmpty {
                    self?.app_showInfoAlert(errorMessage)
                }
            }
        })
    }

    @IBAction func backAction(_ sender: Any) {
        back()
    }

}
