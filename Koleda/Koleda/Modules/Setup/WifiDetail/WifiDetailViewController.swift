//
//  WifiDetailViewController.swift
//  Koleda
//
//  Created by Oanh tran on 12/4/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift

class WifiDetailViewController: BaseViewController, BaseControllerProtocol {

    @IBOutlet weak var wifiSSIDTextField: AndroidStyleTextField!
    @IBOutlet weak var passwordTextField: AndroidStyleTextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var viewModel: WifiDetailViewModelProtocol!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        addCloseFunctionality(UIColor.black)
        viewModel.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func showOrHidePass(_ sender: Any) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    private func configurationUI() {
        Style.Button.primary.apply(to: saveButton)
        viewModel.ssidText.asObservable().bind(to: wifiSSIDTextField.rx.text).disposed(by: disposeBag)
        wifiSSIDTextField.rx.text.orEmpty.bind(to: viewModel.ssidText).disposed(by: disposeBag)
        viewModel.wifiPassText.asObservable().bind(to: passwordTextField.rx.text).disposed(by: disposeBag)
        passwordTextField.rx.text.orEmpty.bind(to: viewModel.wifiPassText).disposed(by: disposeBag)
        viewModel.ssidErrorMessage.asObservable().subscribe(onNext: { [weak self] message in
            self?.wifiSSIDTextField.errorText = message
            if message.isEmpty {
                self?.wifiSSIDTextField.showError(false)
            } else {
                self?.wifiSSIDTextField.showError(true)
            }
        }).disposed(by: disposeBag)
        saveButton.rx.tap.bind { [weak self] _ in
            self?.viewModel.saveWifiInfo()
        }.disposed(by: disposeBag)
        viewModel.disableCloseButton.asObservable().subscribe(onNext: { [weak self] disable in
            self?.closeButton?.isEnabled = !disable
        }).disposed(by: disposeBag)
    }
}
