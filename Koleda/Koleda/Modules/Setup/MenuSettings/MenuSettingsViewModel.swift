//
//  MenuSettingsViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 9/9/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SVProgressHUD

protocol MenuSettingsViewModelProtocol: BaseViewModelProtocol {
    func logOut()
    var profileImage: Driver<UIImage?> { get }
    var settingItems: Variable<[SettingMenuItem]> { get }
    var userName: Variable<String> { get }
    var email: Variable<String> { get}
    var energyConsumed: Variable<String> { get}
    
    func viewWillAppear()
    func selectedItem(at index: Int)
    func showConfigurationScreen(selectedRoom: Room)
}

class MenuSettingsViewModel: BaseViewModel, MenuSettingsViewModelProtocol {
    
    var profileImage: Driver<UIImage?> { return profileImageSubject.asDriver(onErrorJustReturn: nil) }
    
    let settingItems = Variable<[SettingMenuItem]>([])
    let userName =  Variable<String>("")
    let email =  Variable<String>("")
    let energyConsumed = Variable<String>("0.0")
    
    private var profileImageSubject = BehaviorSubject<UIImage?>(value: UIImage(named: "defaultProfileImage"))
    
    let router: BaseRouterProtocol
    private let userManager: UserManager
    private let settingManager: SettingManager
    
    func viewWillAppear() {
        settingItems.value = SettingMenuItem.initSettingMenuItems()
        guard let name = UserDataManager.shared.currentUser?.name else {
            return
        }
        userName.value = name
        guard let emailOfUser = UserDataManager.shared.currentUser?.email else {
            return
        }
        email.value = emailOfUser
        getEnergyConsumedInfo()
    }
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router =  router
        settingManager =  managerProvider.settingManager
        userManager =  managerProvider.userManager
        super.init(managerProvider: managerProvider)
    }
    
    func logOut() {
        SVProgressHUD.show()
        userManager.logOut { [weak self] in
            SVProgressHUD.dismiss()
            self?.router.enqueueRoute(with: MenuSettingsRouter.RouteType.logOut)
        }
    }
    
    func selectedItem(at index: Int) {
        switch index {
        case 0:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.roomsConfiguration)
        case 1:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.addRoom)
        case 2:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.smartScheduling)
        case 3:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.updateTariff)
        case 4:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.wifiDetail)
        case 5:
            router.enqueueRoute(with: MenuSettingsRouter.RouteType.modifyModes)
        default:
            break
        }
    }
    
    func showConfigurationScreen(selectedRoom: Room) {
        router.enqueueRoute(with: MenuSettingsRouter.RouteType.configuration(selectedRoom))
    }
    
    private func getTariffInfo(completion: @escaping () -> Void) {
        settingManager.getTariff(success: { [weak self] in
            completion()
        }) { _ in
            completion()
        }
    }
    
    private func getEnergyConsumedInfo() {
        getTariffInfo { [weak self] in
            self?.settingManager.getEnergyConsumed(success: { [weak self] in
                guard let currencySymbol = UserDataManager.shared.tariff?.currency.kld_getCurrencySymbol() else {
                    return
                }
                let amount = UserDataManager.shared.energyConsumed
                self?.energyConsumed.value = "\(currencySymbol)\(amount.roundToDecimal(2))"
            }) { _ in
            }
        }
    }
    
}

struct SettingMenuItem {
    let title: String
    let icon: UIImage?
    
    init(title: String, icon: UIImage?) {
        self.title = title
        self.icon = icon
    }
    
    static func initSettingMenuItems() -> [SettingMenuItem] {
        var settingMenuItems: [SettingMenuItem] = []
        settingMenuItems.append(SettingMenuItem(title: "Room Configuration", icon: UIImage(named: "configRoomMenuItem")))
        settingMenuItems.append(SettingMenuItem(title: "Add Room", icon: UIImage(named: "addRoomMenuItem")))
        settingMenuItems.append(SettingMenuItem(title: "Smart Scheduling", icon: UIImage(named: "tarrifMenuItem")))
        settingMenuItems.append(SettingMenuItem(title: "Tariff", icon: UIImage(named: "tarrifMenuItem")))
        settingMenuItems.append(SettingMenuItem(title: "Wifi Setting", icon: UIImage(named: "tarrifMenuItem")))
        settingMenuItems.append(SettingMenuItem(title: "Modify Temperature modes", icon: UIImage(named: "tarrifMenuItem")))
        return settingMenuItems
    }
}
