//
//  TemperatureUnitViewController.swift
//  Koleda
//
//  Created by Oanh tran on 9/17/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift

class TemperatureUnitViewController: BaseViewController, BaseControllerProtocol {

    @IBOutlet weak var cUnitTempView: TemperatureUnitView!
    @IBOutlet weak var fUnitTempView: TemperatureUnitView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cesiusButton: UIButton!
    @IBOutlet weak var fahrenheitButton: UIButton!
    
    var viewModel: TemperatureUnitViewModelProtocol!
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        addCloseFunctionality()
        setTitleScreen(with: "")
        viewModel.viewWillAppear()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configurationUI() {
        Style.View.shadowCornerWhite.apply(to: cUnitTempView)
        Style.View.shadowCornerWhite.apply(to: fUnitTempView)
        cUnitTempView.setUp(unit: .C)
        fUnitTempView.setUp(unit: .F)
        viewModel.seletedUnit.asObservable().subscribe(onNext: { [weak self] unit in
            self?.cUnitTempView.updateStatus(enable: unit == .C)
            self?.fUnitTempView.updateStatus(enable: unit == .F)
        }).disposed(by: disposeBag)
        viewModel.hasChanged.asObservable().subscribe(onNext: { [weak self] changed in
            if changed {
                NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
            } else {
                self?.app_showAlertMessage(title: "Error", message: "Can't Update Temperature Unit. Please try again later")
            }
        }).disposed(by: disposeBag)
        confirmButton.rx.tap.bind { [weak self]  in
            guard let `self` = self else {
                return
            }
            self.viewModel.confirmedAndFinished()
        }.disposed(by: disposeBag)
        
        cesiusButton.rx.tap.bind {
            self.viewModel.updateUnit(selectedUnit: .C)
        }.disposed(by: disposeBag)
        
        fahrenheitButton.rx.tap.bind {
            self.viewModel.updateUnit(selectedUnit: .F)
        }.disposed(by: disposeBag)
        
    }
}
