//
//  TermAndConditionViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 7/3/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import RxSwift
import SVProgressHUD

protocol TermAndConditionViewModelProtocol: BaseViewModelProtocol {
    func showLocationScreen()
    func showWifiScreen()
}

class TermAndConditionViewModel: BaseViewModel, TermAndConditionViewModelProtocol {
    
    let router: BaseRouterProtocol
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        super.init(managerProvider: managerProvider)
    }
    
    func showLocationScreen() {
        self.router.enqueueRoute(with: TermAndConditionRouter.RouteType.location)
    }
    
    func showWifiScreen() {
        self.router.enqueueRoute(with: TermAndConditionRouter.RouteType.wifi)
    }
    
}
