//
//  InstructionView.swift
//  Koleda
//
//  Created by Vu Xuan Hoa on 9/14/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import SwiftRichString

enum TypeInstruction {
    case instructionForSensor1
    case instructionForSensor2
    case instructionForSensor3
    case instructionForSensor4
    case instructionForSensor5
    case instructionForSensor6
    case instructionForHeater1
    case instructionForHeater2
    case instructionForHeater3
    case instructionForHeater4
}

class InstructionView: UIView {
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var instructionImageView: UIImageView!
    @IBOutlet weak var heighInstructionConstraint: NSLayoutConstraint!
    @IBOutlet weak var titelLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UITextView!
    var styleGroup:StyleGroup!
    var styleGroupForSubTitle:StyleGroup!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        kld_loadContentFromNib()
        initView()
    }
    
    func initView() {
        var fontSize: CGFloat = 24
        if UIDevice.screenType == .iPhones_5_5s_5c_SE {
            fontSize = 16
            heighInstructionConstraint.constant = 275
        }
        
        let normal = SwiftRichString.Style {
            $0.font = UIFont.app_SFProDisplaySemibold(ofSize: fontSize)
            $0.color = UIColor.hex1F1B15
        }
        
        let bold = SwiftRichString.Style {
            $0.font = UIFont.app_SFProDisplaySemibold(ofSize: fontSize)
            $0.color = UIColor.hexFF7020
        }
        
        let normalSub = SwiftRichString.Style {
            $0.font = UIFont.app_SFProDisplayRegular(ofSize: 14)
            $0.color = UIColor.gray
        }
        
        let boldAndBlack = SwiftRichString.Style {
            $0.font = UIFont.app_SFProDisplaySemibold(ofSize: 14)
            $0.color = UIColor.black
        }
        
        styleGroup = StyleGroup(base: normal, ["h": bold])
        styleGroupForSubTitle = StyleGroup(base: normalSub, ["b" : boldAndBlack])
    }
    
    func updateUI(type: TypeInstruction) {
        var title: String
        var subTitle: String
        var instructionImage: String
        var backgroundColor: UIColor
        switch type {
        case .instructionForSensor1:
            backgroundColor = UIColor(hex: 0x252525)
            instructionImage = "ic-instruction-sensor-1"
            title = "Caution when setting a room,\nonly turn on <h>one sensor</h>\nat a time."
            subTitle = "<b>Warning:</b> In order to make setting up your new SOLUS+ home system as easy as possible, we recommend setting up each heater and sensor individually, one at a time. We recommend setting up the sensor in the exact location of installation to ensure a stable wifi connection."
        case .instructionForSensor2:
            backgroundColor = UIColor(hex: 0x252525)
            instructionImage = "ic-instruction-sensor-2"
            title = "Open sensor by turning the <h>top part of the casing</h> 45 degrees."
            subTitle = "Open up the SOLUS temperature sensor by turning the top half of the outer casing 45 degrees clockwise. Make sure the SOLUS temperature sensor is powered using a CR123A battery."
        case .instructionForSensor3:
            backgroundColor = UIColor(hex: 0x252525)
            instructionImage = "ic-instruction-sensor-3"
            title = "Turn sensor by <h>pressing down</h>\nthe button."
            subTitle = "Press down the Button - the LED should turn on and flash slowly.\n<b>Warning</b>: If the LED does not flash slowly, press and hold the button for at least 10 seconds. The LED should then flash quickly signaling that the temperature sensor has been reset."
        case .instructionForSensor4:
            backgroundColor = UIColor(hex: 0x252525)
            instructionImage = "ic-instruction-sensor-4"
            title = "Close sensor by reattaching <h>casing</h> and twisting 45 degrees."
            subTitle = "You can close the sensor by realigning the top and bottom section of the outer casing and turning 45 degrees anti-clockwise."
        case .instructionForSensor5:
            backgroundColor = UIColor(hex: 0x252525)
            instructionImage = "ic-instruction-sensor-5"
            title = "Connect directly to SOLUS temperature sensor via wifi."
            subTitle = "To do this simply: Open Settings > WiFi and connect to the WiFi network created by SOLUS temperature sensor, e.g. solus-35FA58."
        case .instructionForSensor6:
            backgroundColor = UIColor(hex: 0xE6EAF0)
            instructionImage = "ic-instruction-sensor-6"
            title = "Connect to Sensor Wi-fi\naccess point."
            subTitle = "1. Connect directly to the SOLUS local wifi connection by connecting to shellyht-XXXXXX or Solus-Sensor-XXXXXX in your wifi settings.\n2. Once connected, return to the SOLUS+ app and input the wifi credentials of your home network.\n3. Once you have inputted the correct credentials, rejoin your home network and if successful, the app will detect the SOLUS sensor."
        case .instructionForHeater1:
            backgroundColor = UIColor(hex: 0xE6EAF0)
            instructionImage = "ic-instruction-heater-1"
            title = "Caution when setting a room,\nonly turn on one heater at a time."
            subTitle = "<b>Warning:</b>\nIn order to make setting up your new SOLUS+ home system as easy as possible, we recommend setting up each heater and sensor individually, one at a time. We recommend setting up the heater in the exact location of installation to ensure a stable wifi connection."
        case .instructionForHeater2:
            backgroundColor = UIColor(hex: 0xE6EAF0)
            instructionImage = "ic-instruction-heater-2"
            title = "Plug Solus in."
            subTitle = "Connect SOLUS+ to a power outlet and ensure SOLUS+ is fixed firmly in place using the included wall brackets. When SOLUS+ is connected to the power outlet, press the power button on the side of SOLUS+ to start the set-up procedure. If SOLUS+ is correctly powered, the power button will light up."
        case .instructionForHeater3:
            backgroundColor = UIColor(hex: 0xE6EAF0)
            instructionImage = "ic-instruction-heater-3"
            title = "Connect to Heater Wi-fi\naccess point."
            subTitle = "Connect directly to SOLUS+ via wifi. To do this simply:\nOpen Settings > WiFi and connect to the WiFi network created by SOLUS, e.g. solus-35FA58.\n<b>Warning:</b> If SOLUS+ does not appear, unplug the device. Re-connect to the power outlet and quickly press the power button on and off 5 times within 30secs. This will reset the wireless settings."
        case .instructionForHeater4:
            backgroundColor = UIColor(hex: 0xE6EAF0)
            instructionImage = "ic-instruction-heater-4"
            title = "Connect to Heater Wi-fi\naccess point."
            subTitle = "1. Connect directly to the SOLUS local wifi connection by connecting to SOLUS-Heater-XXXXXX or Shelly1pm-XXXXXX in your wifi settings.\n2. Once connected, return to the SOLUS+ app and input the wifi credentials of your home network.\n3. Once you have inputted the correct credentials, rejoin your home network and if successful, the app will detect the SOLUS heater."
        }
        backgroundView.backgroundColor = backgroundColor
        instructionImageView.image = UIImage(named:instructionImage)
        titelLabel.attributedText = title.set(style: styleGroup)
        subTitleLabel.attributedText = subTitle.set(style: styleGroupForSubTitle)
    }
}
